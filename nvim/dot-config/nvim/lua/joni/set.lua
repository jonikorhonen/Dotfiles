vim.opt.compatible = false

vim.opt.guicursor = ""

vim.opt.cursorline = true

vim.opt.nu = true
vim.opt.relativenumber = true
vim.opt.cpoptions = "ces$"

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.wrap = true--false
vim.opt.showbreak = "↳"

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.smartcase = true
vim.opt.ignorecase = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "80"

vim.o.lcs = vim.o.lcs
              .. "," .. "space:·"
              .. "," .. "tab:⟶ "
              .. "," .. "eol:↴"
              .. "," .. "trail:-"
              .. "," .. "nbsp:%"
vim.o.list = false
