require("joni.set")
require("joni.remap")
require("joni.commands")

local augroup = vim.api.nvim_create_augroup
local MyGroup = augroup("My", {})

local autocmd = vim.api.nvim_create_autocmd
local yank_group = augroup("HighlightYank", {})

autocmd("TextYankPost", {
    group = yank_group,
    pattern = "*",
    callback = function()
        vim.highlight.on_yank({
            higroup = "IncSearch",
            timeout = 40,
        })
    end,
})

-- Remove whitespce from line ends
autocmd({"BufWritePre"}, {
    group = MyGroup,
    pattern = "*",
    command = [[%s/\s\+$//e]],
})

-- Makefiles must use tabs instead of spaces
autocmd("FileType", {
    group = MyGroup,
    pattern = "Makefile",
    callback = function()
        vim.opt_local.expandtab = false
    end
})

-- VimWiki diary template handling
autocmd({"BufNewFile"}, {
    group = MyGroup,
    pattern = { "*diary/*.md" },
    command = "0r! ~/.local/bin/vimwiki-diary-tpl.py",
})

-- Attempt at auto sourcing configs on write
-- autocmd({"BufWritePost"}, {
--     group = MyGroup,
--     pattern = "~/.config/nvim/lua/joni/*",
--     command = "source ~/.config/nvim/lua/joni/init.lua",
-- })

vim.g.netrw_browse_split = 0
vim.g.netrw_banner = 1
vim.g.netrw_winsize = 25
