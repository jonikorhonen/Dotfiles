vim.g.mapleader = " "

local setkey = vim.keymap.set

-- space is map leader, ensure it does nothing else
setkey('', '<Space>', '<Nop>')

-- Open vim explorer
-- % - create new file
-- d - create new directory
-- D - delete entry under cursor
setkey("n", "<leader>pv", vim.cmd.Ex)

-- Move highlighted selection up down
setkey("v", "J", ":m '>+1<CR>gv=gv")
setkey("v", "K", ":m '<-2<CR>gv=gv")

-- Append line below to current line with space as delimiter, keep cursor in place
setkey("n", "J", "mzJ`z")

-- Keep cursor in the middle when moving between search results
setkey("n", "n", "nzzzv")
setkey("n", "N", "Nzzzv")
-- More logical movement around wrapped lines
setkey("n", "j", "gj")
setkey("n", "k", "gk")
-- Keep cursor in the middle when jumping halfpage up/down
setkey("n", "<C-d>", "<C-d>zz")
setkey("n", "<C-u>", "<C-u>zz")

-- Move to window with <Ctrl-hjkl> currently clashes with harpoon
--setkey("n", "<C-h>", "<C-w>h", { desc = "Go to left window" })
--setkey("n", "<C-j>", "<C-w>j", { desc = "Go to lower window" })
--setkey("n", "<C-k>", "<C-w>k", { desc = "Go to upper window" })
--setkey("n", "<C-l>", "<C-w>l", { desc = "Go to right window" })

-- Resize window with <Ctrl-arrow keys>
setkey("n", "<C-Up>", "<cmd>resize +2<CR>", { desc = "Increase window height" })
setkey("n", "<C-Down>", "<cmd>resize -2<CR>", { desc = "Decrease window height" })
setkey("n", "<C-Left>", "<cmd>vertical resize -2<CR>", { desc = "Decrease window width" })
setkey("n", "<C-Right>", "<cmd>vertical resize +2<CR>", { desc = "Increase window width" })

-- Split window
setkey("n", "<leader>sv", "<cmd>vsplit<CR>", { desc = "Split window vertically" })
setkey("n", "<leader>sh", "<cmd>split<CR>", { desc = "Split window horizontally" })

-- Keep current yanked selection when copying over new selection
setkey("x", "<leader>p", [["_dP]], { desc = "Keep yanked selection when copying over new selection" })
-- Yank to system clipboard
setkey({"n", "v"}, "<leader>y", [["+y]], { desc = "Yank selection to system clipboard" })
-- Yank current row to system clipboard
setkey("n", "<leader>yy", [["+Y]], { desc = "Yank row to system clipboard" })

setkey("n", "Q", "<nop>")
setkey("n", "<C-f>", "<cmd>silent !tmux neww tmux-sessionizer<CR>")

-- Replace all instances of word under cursor in file
setkey("n", "<leader>s", [[:%s/<C-r><C-w>/<C-r><C-w>/gI<Left><Left><Left>]])
-- Replace all instances of visual selection in file
setkey("v", "<leader>s", "y:%s/<C-r>0/<C-r>0/gI<Left><Left><Left>")
-- Make current file executable
setkey("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

-- setkey("n", "<leader>f", vim.lsp.buf.format)

-- GO helpers
setkey(
    "n",
    "<leader>ee",
    "oif err != nil {<CR>}<Esc>Oreturn err<Esc>"
)

setkey("n", "<S-h>", "<cmd>bprev<CR>", { desc = "Switch to previous buffer" })
setkey("n", "<S-l>", "<cmd>bnext<CR>", { desc = "Switch to next buffer" })

-- Toggle list (display unprintable characters)
setkey("n", "<F2>", "<cmd>set list!<CR>")
setkey("i", "<F2>", "<esc><cmd>set list!<CR>a")

setkey("t", "qq", "<C-\\><C-n>", { desc = "Exit terminal mode without closing the terminal" })

setkey("v", "<leader>@", ": norm @", { desc = "Apply macro over visual selection" })

setkey("n", "<leader>fml", "<cmd>CellularAutomaton make_it_rain<CR>", { desc = "FML" })
