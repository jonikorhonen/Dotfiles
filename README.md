# My dotfiles

Contains dotfiles and other useful resources for my systems.

Dotfiles managed with [GNU stow](https://www.gnu.org/software/stow).

## Requirements

Ensure you have at minimum following installed in your system.

### Git

```bash
pacman -S git
```

### Stow

```bash
pacman -S stow
```

## Usage

Checkout dotfiles repo to $HOME directory using git

```bash
git clone git@gitlab.com:jonikorhonen/dotfiles.git
cd dotfiles
```

## Dotfiles

Use helper script to create all configuration symlinks with GNU stow:

```bash
./init.sh
```

Or selectively run for subset of stowed files, e.g. zsh related configurations:

```bash
stow --verbose --dotfiles zsh
```

## Application specific setup

Application specific setup needed by configurations.

### Nvim

Packer plugin manager needs to be present

```bash
git clone --depth 1 https://github.com/wbthomason/packer.nvim\ ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

Jdtls needs lombok.jar to function

```bash
mkdir -p .local/share/nvim/mason/packages/jdtls && cd $_
wget -O lombok.jar https://projectlombok.org/downloads/lombok.jar
```

### Tmux

Install tmux plugin manager

```bash
git clone --depth=1 https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

### Zsh

I use oh-my-zsh, so lets install it

```bash
sh -c "$(wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

## System installation

### Arch linux

[ArchWiki - Installation guide](https://wiki.archlinux.org/title/Installation_guide)

TODO
